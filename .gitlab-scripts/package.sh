#!/bin/bash

set -euo pipefail
set -x

if echo "${CI_COMMIT_TAG}"|grep -E '^(v|rc-|alpha-|beta-)[0-9]+\.[0-9]+\.[0-9]+'; then
  if echo "${CI_COMMIT_TAG}"|grep -E '^v'; then
    BASE="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/token-action-hud-od6s"
  else
    POST="$(echo ${CI_COMMIT_TAG}|sed 's/^\([a-z]*\)-.*/\1/')"
    BASE="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/token-action-hud-od6s-${POST}"
  fi
  VERSION=$(echo "${CI_COMMIT_TAG}" | sed 's/[^0-9.]*\([0-9.]*\).*/\1/')
  REL="${BASE}/${VERSION}"
  ZIP="${REL}/token-action-hud-od6s.zip"
  MANIFEST="${REL}/module.json"
  sed -i "s/\"version\":.*/\"version\": \"${VERSION}\",/" module.json
  sed -i "s#\"manifest\":.*#\"manifest\": \"${MANIFEST}\",#" module.json
  sed -i "s#\"download\":.*#\"download\": \"${ZIP}\",#" module.json
fi

cat module.json

apt-get update -y
apt-get install zip -y
mkdir -p build/token-action-hud-od6s
cp -r languages build/token-action-hud-od6s
cp -r scripts build/token-action-hud-od6s
cp module.json build/token-action-hud-od6s
mkdir -p build/artifacts
cd build
zip -r artifacts/token-action-hud-od6s.zip token-action-hud-od6s/
ls -al artifacts
